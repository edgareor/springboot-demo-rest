package demo.api.service;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;

import org.springframework.stereotype.Controller;

@Controller
@Path("/api-rest")
public class DemoRest {
	
	@GET
	@Path("/fechas/actual")
	@Produces({ "application/json" })
	public Map<String, Object> getStatus() {
		
		Date fecha = new Date ();
		Locale currentLocale = new Locale("EN");
		DateFormat formato = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM, currentLocale);
		String output = formato.format(fecha);
		
		Map<String, Object> collection = new HashMap<String, Object>();
		collection.put("GetStatus", output);

         	return collection;
	}
}
