package demo.api;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import demo.api.service.DemoRest;

@Configuration 
@ApplicationPath("/servlet")				// A este nivel de ruta ya podemos generar el WADL con /application.wadl
public class JerseyConfiguration extends ResourceConfig {

    @Autowired
    public JerseyConfiguration(){   	
        register(DemoRest.class);
    }
}
