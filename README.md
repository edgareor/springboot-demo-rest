# springboot-demo-rest
Servicio REST - Spring Boot, solo método GET (:8384/api-rest/fechas/actual). Retorna Fecha Actual. Para usarlo simplemente realice un "$git clone" seguido de un "$mvn package" para generar el .war y por ultimo ejecutar con "$java -jar compilado.war".

Version 2.0.0.0: Solamente metodo GET de obtener fechas.
